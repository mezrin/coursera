package com.victormezrin.coursera.posa.week4;

/**
 * User: Victor Mezrin
 * Date: 2013-04-04
 * Time: 3:39 PM
 */
interface IPlayerObserver {

    /**
     * Used to notify concrete observer that event "player hits the ball" happened
     */
    public void update();
}
