package com.victormezrin.coursera.posa.week5;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * User: Victor Mezrin
 * Date: 2013-04-15
 * Time: 12:29 AM
 */
public class ChopstickManager {

    public ChopstickManager(List<Philosopher> philosophers) {
        this.philosophers = philosophers;

        /** initialize Chopsticks */
        int dinnerParties = this.philosophers.size();
        this.chopsticks = new ArrayList<Chopstick>(dinnerParties);
        for (int i = 0; i < dinnerParties; i++) {
            this.chopsticks.add(new Chopstick());
        }

        this.chopstickLocks = new Hashtable<Chopstick, Philosopher>();
    }

//----------------------------------------------------------------------------------------------------------------------
//  Public interface
//----------------------------------------------------------------------------------------------------------------------

    public Chopstick acquireLeftChopstick(Philosopher philosopher) {
        Chopstick chopstick = this.getLeftChopstick(philosopher);
        this.acquireChopstick(chopstick, philosopher);
        return chopstick;
    }

    public Chopstick acquireRightChopstick(Philosopher philosopher) {
        Chopstick chopstick = this.getRightChopstick(philosopher);
        this.acquireChopstick(chopstick, philosopher);
        return chopstick;
    }

    public void releaseLeftChopstick(Philosopher philosopher) {
        this.releaseChopstick(this.getLeftChopstick(philosopher));
    }

    public void releaseRightChopstick(Philosopher philosopher) {
        this.releaseChopstick(this.getRightChopstick(philosopher));
    }

//----------------------------------------------------------------------------------------------------------------------
//  Management of concurrency
//----------------------------------------------------------------------------------------------------------------------

    /**
     * Acquires shared resource
     * Chopstick may be acquired if: not acquired by another philosopher and deadlock not occurs after acquiring
     *
     * @param chopstick Chopstick to be acquired
     * @param philosopher Resource consumer
     */
    private synchronized void acquireChopstick(Chopstick chopstick, Philosopher philosopher) {
        while (true) {
            if (this.isChopstickLocked(chopstick) || this.isDeadlockOccurs(philosopher)) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                this.lockChopstick(chopstick, philosopher);
                return;
            }
        }
    }

    private synchronized void releaseChopstick(Chopstick chopstick) {
        this.unlockChopstick(chopstick);
        notifyAll();
    }

//----------------------------------------------------------------------------------------------------------------------
//  Management of resources
//----------------------------------------------------------------------------------------------------------------------

    private boolean isChopstickLocked(Chopstick chopstick) {
        return this.chopstickLocks.get(chopstick) != null;
    }

    /**
     * Deadlock is possible if:
     * only one not locked chopstick exists
     * current philosopher does not have a chopstick locked before, no one philosopher has 2 chopsticks
     *
     * @param philosopher Resource consumer that is going to lock chopstick
     * @return True if deadlock occurs after the chopstick will be locked, otherwise False
     */
    private boolean isDeadlockOccurs(Philosopher philosopher) {
        if (this.chopsticks.size() - this.chopstickLocks.size() > 1) {
            return false;
        } else {
            List<Philosopher> chopstickHolders = new ArrayList<Philosopher>();
            chopstickHolders.add(philosopher);
            for (Map.Entry<Chopstick, Philosopher> entry : this.chopstickLocks.entrySet()) {
                if (chopstickHolders.contains(entry.getValue())) {
                    return false;
                }
            }
            return true;
        }
    }

    private void lockChopstick(Chopstick chopstick, Philosopher philosopher) {
        this.chopstickLocks.put(chopstick, philosopher);
    }

    private void unlockChopstick(Chopstick chopstick) {
        this.chopstickLocks.remove(chopstick);
    }

    private Chopstick getLeftChopstick(Philosopher philosopher) {
        int chopstickPosition = this.getPhilosopherPosition(philosopher) - 1;
        if (chopstickPosition < 0) {
            chopstickPosition += this.chopsticks.size();
        }
        return this.chopsticks.get(chopstickPosition);
    }

    private Chopstick getRightChopstick(Philosopher philosopher) {
        int chopstickPosition = this.getPhilosopherPosition(philosopher);
        return this.chopsticks.get(chopstickPosition);
    }

    private int getPhilosopherPosition(Philosopher philosopher) {
        int placement = 0;
        for (Philosopher p : this.philosophers) {
            if (p == philosopher) { //exactly the same object
                break;
            }
            placement++;
        }
        if (placement >= this.philosophers.size()) {
            throw new IllegalArgumentException("Philosopher late for dinner");
        }
        return placement;
    }

//----------------------------------------------------------------------------------------------------------------------
//  Fields
//----------------------------------------------------------------------------------------------------------------------

    /**
     * List of dinner parties
     */
    private List<Philosopher> philosophers;

    /**
     * List of chopsticks
     * Philosopher [0] uses chopsticks [last] and [0], Philosopher [1] uses chopsticks [0] and [1], etc.
     * Philosopher [last] uses chopsticks [last - 1] and [last]
     */
    private List<Chopstick> chopsticks;

    /**
     * Map "Chopstick -> Philosopher that locked this chopstick"
     */
    private Map<Chopstick, Philosopher> chopstickLocks;
}
