package com.victormezrin.coursera.posa.week5;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Victor Mezrin
 * Date: 2013-04-14
 * Time: 11:53 PM
 */
public class DiningPhilosophersProgram {

    public static void main(String[] args) throws Exception{
        /** initial settings */
        int numberOfPhilosophers = 5;
        int numberOfEating = 5;

        /** philosophers and resource manager configuration */
        List<Philosopher> philosophers = new ArrayList<Philosopher>(numberOfPhilosophers);
        for(int i = 0; i < numberOfPhilosophers; i++){
            Philosopher philosopher = new Philosopher();
            philosopher.setName("Philosopher " + i);
            philosopher.setEatingLeft(numberOfEating);
            philosophers.add(philosopher);
        }

        ChopstickManager manager = new ChopstickManager(philosophers);

        for (Philosopher p: philosophers){
            p.setManager(manager);
        }

        /** start  of threads */
        System.out.println("Dinner is starting!");

        List<Thread> threads = new ArrayList<Thread>(numberOfPhilosophers);
        for(int i = 0; i < numberOfPhilosophers; i++){
            threads.add(new Thread(philosophers.get(i)));
        }

        for(Thread thread: threads){
            thread.start();
        }

        for(Thread thread: threads){
            thread.join();
        }

        System.out.println("Dinner is over!");
    }
}
