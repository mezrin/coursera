package com.victormezrin.coursera.posa.week6.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * User: Victor Mezrin
 * Date: 4/21/13
 * Time: 5:52 PM
 */
public class ClientApp {
    String hostIp;
    int hostPort;

    public ClientApp(String hostIp, int hostPort) {
        this.hostIp = hostIp;
        this.hostPort = hostPort;
    }

    public void runClient() throws IOException {
        Socket clientSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            clientSocket = new Socket(hostIp, hostPort);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Unknown host: " + hostIp);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't connect to: " + hostIp);
            System.exit(1);
        }

        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        String userInput;

        System.out.println("You connected to host: " + hostIp + " port: " + hostPort);
        System.out.println("Talk to Server.....");
        System.out.println("Type (\"Bye\" to quit)");

        while ((userInput = stdIn.readLine()) != null) {
            System.out.println("You said: " + userInput);
            out.println(userInput);

            // Break when user says Bye.
            if (userInput.equalsIgnoreCase("Bye")){
                break;
            }

            System.out.println("Server said: " + in.readLine());
        }

        System.out.println("Stop conversation.");

        out.close();
        in.close();
        stdIn.close();
        clientSocket.close();
    }

    public static void main(String[] args) throws IOException {
        if(args == null || args.length == 0){
            System.out.println("Port number is not specified");
            System.exit(1);
        }
        int port = Integer.parseInt(args[0]);
        if (port < 1024 || port > 65535){
            System.out.println("Wrong port number: " + port);
            System.exit(1);
        }

        ClientApp client = new ClientApp("127.0.0.1", port);
        client.runClient();

        System.out.println("Finished");
    }
}
