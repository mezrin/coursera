package com.victormezrin.coursera.posa.week6;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Arrays;

/**
 * User: Victor Mezrin
 * Date: 4/21/13
 * Time: 5:53 PM
 */
public class EventHandler implements Runnable {

    EventHandler(Selector selector, SocketChannel socketChannel) throws IOException {
        this.socketChannel = socketChannel;
        this.socketChannel.configureBlocking(false);
        this.selectionKey = this.socketChannel.register(selector, SelectionKey.OP_READ, this);
    }

    @Override
    public void run() {
        try {
            if (this.state == EventHandler.Status.READING) {
                read();
            } else if (this.state == EventHandler.Status.SENDING) {
                send();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Reading client message
     *
     * @throws IOException
     */
    void read() throws IOException {
        int readCount = this.socketChannel.read(this.input);

        if (readCount == -1) {
            this.socketChannel.close();
            System.out.println("Stream is closed. Close connection.");
            return;
        }

        if (readCount > 0) {
            processMessage(readCount);
        }

        if (this.clientMessage.equalsIgnoreCase("Bye")) {
            this.socketChannel.close();
            System.out.println("Client said Bye. Close connection.");
            return;
        }

        this.state = EventHandler.Status.SENDING;
        this.selectionKey.interestOps(SelectionKey.OP_WRITE); //mark that we interested in writing
    }

    /**
     * Processing of the read message.
     *
     * @param readCount Number of bytes to read
     */
    synchronized void processMessage(int readCount) {
        this.input.flip();
        StringBuilder sb = new StringBuilder();
        sb.append(new String(Arrays.copyOfRange(input.array(), 0, readCount))); // Assuming ASCII (bad assumption but simplifies the example)
        this.clientMessage = sb.toString().trim();
        this.input.clear();
        System.out.println("Client said: " + this.clientMessage);
    }

    /**
     * Sending response to client
     *
     * @throws IOException
     */
    void send() throws IOException {
        System.out.println("Answer to client: " + this.clientMessage);
        this.socketChannel.write(ByteBuffer.wrap((this.clientMessage + "\n").getBytes()));
        this.state = EventHandler.Status.READING;
        this.selectionKey.interestOps(SelectionKey.OP_READ); //mark that we interested in reading
    }

//----------------------------------------------------------------------------------------------------------------------
//  Fields
//----------------------------------------------------------------------------------------------------------------------

    final SocketChannel socketChannel;
    final SelectionKey selectionKey;

    ByteBuffer input = ByteBuffer.allocate(1024);
    EventHandler.Status state = EventHandler.Status.READING;
    String clientMessage = "";

//----------------------------------------------------------------------------------------------------------------------
//  Enum to mark current status of EventHandler
//----------------------------------------------------------------------------------------------------------------------

    enum Status {
        READING, SENDING
    }
}
