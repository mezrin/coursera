package com.victormezrin.coursera.posa.week4;

/**
 * User: Victor Mezrin
 * Date: 2013-04-04
 * Time: 3:26 PM
 */
public class PingPongGame {
    public static void main(String[] args) throws Exception {
        /**
         * Creates new runnable objects
         * Player 1 hits first
         */
        Player player1 = new Player(true);
        Player player2 = new Player(false);
        player1.setConsoleText("Ping!");
        player2.setConsoleText("Pong!");

        /**
         * Register players as observers of each other
         * It is an easy and general way to inform the player that he could hit the ball
         */
        player1.attachObserver(player2);
        player2.attachObserver(player1);

        /**
         * Create threads and start game
         */
        Thread thread1 = new Thread(player1);
        Thread thread2 = new Thread(player2);

        System.out.println("Ready... Set... Go!");

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        System.out.println("Done!");
    }
}
