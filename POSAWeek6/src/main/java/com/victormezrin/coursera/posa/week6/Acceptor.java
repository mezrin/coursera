package com.victormezrin.coursera.posa.week6;

import java.io.IOException;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * User: Victor Mezrin
 * Date: 4/21/13
 * Time: 6:04 PM
 */
public class Acceptor implements Runnable {

    final ServerSocketChannel serverSocketChannel;
    final Selector selector;

    public Acceptor(ServerSocketChannel serverSocketChannel, Selector selector) {
        this.serverSocketChannel = serverSocketChannel;
        this.selector = selector;
    }

    public void run() {
        try {
            SocketChannel socketChannel = this.serverSocketChannel.accept();
            if (socketChannel != null) {
                new EventHandler(this.selector, socketChannel);
                System.out.println("Connection Accepted");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
