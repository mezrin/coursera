package com.victormezrin.coursera.posa.week4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * User: Victor Mezrin
 * Date: 2013-04-04
 * Time: 3:26 PM
 */
class Player implements Runnable, IPlayerObserver, IObservablePlayer {

    /**
     * Configures new object
     *
     * @param isServer True if player starts the game
     */
    public Player(boolean isServer) {
        this.gate = new Semaphore(isServer ? 1 : 0);
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted() && this.hitsLeft > 0) {
            try {
                this.gate.acquire();
                this.hitBall();
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    public void update() {
        this.gate.release();
    }

    @Override
    public void attachObserver(IPlayerObserver observer) {
        this.observers.add(observer);
    }

    /**
     * Sets the text that prints a player after the ball is hit
     *
     * @param text Text to be printed
     */
    void setConsoleText(String text) {
        this.consoleText = text;
    }

    /**
     * Print text to console
     */
    private void printText() {
        System.out.println(this.consoleText);
    }

    /**
     * Hit the ball
     */
    private void hitBall() {
        this.hitsLeft--;
        this.printText();
        this.notifyObservers();
    }

    /**
     * Notify all observers about hit
     */
    private void notifyObservers() {
        for (IPlayerObserver observer : this.observers) {
            observer.update();
        }
    }

    private String consoleText;
    private List<IPlayerObserver> observers = new ArrayList<IPlayerObserver>();
    private Semaphore gate;
    private int hitsLeft = 3; //todo magic number - move it to settings provider
}