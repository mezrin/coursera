package com.victormezrin.coursera.posa.week6;

import java.io.IOException;

/**
 * User: Victor Mezrin
 * Date: 4/21/13
 * Time: 5:53 PM
 */
public class ServerApp {

    public static void main(String[] args) throws IOException {
        if (args == null || args.length == 0) {
            System.out.println("Port number is not specified");
            System.exit(1);
        }
        int port = Integer.parseInt(args[0]);
        if (port < 1024 || port > 65535) {
            System.out.println("Wrong port number: " + port);
            System.exit(1);
        }

        Runnable reactor = new Reactor(port);
        new Thread(reactor).start();
    }
}
