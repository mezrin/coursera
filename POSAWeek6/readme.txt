------------------------------------------------------------------------------------------------------------------------
----- Intro
------------------------------------------------------------------------------------------------------------------------
Java NIO was used. In code you can find some classes:

Reactor - class with the main loop
Acceptor - creates new SocketChannel and EventHandler for new connection
EventHandler - perform reading and sending messages
ServerApp - class with the main method

------------------------------------------------------------------------------------------------------------------------
----- Patterns
------------------------------------------------------------------------------------------------------------------------
Wrapper pattern - we don't need to make new wrapper. Java NIO is a wrapper itself - we don't use low-level socket API

Reactor pattern:
Reactor - class Reactor
Handle - classes from Java NIO: ServerSocketChannel, SocketChannel, Selector, SelectionKey
EventHandler interface - java interface Runnable
Synchronous Event Demuxer - method from Java NIO   Selector.selectedKeys();  (see my class Reactor)

Acceptor-Connector.
We need just to implement Acceptor part of this pattern. It's a class Acceptor.
Interface for this class is - like for EventHandler - java interface Runnable

HalfSync/HalfAsync
HalfSync part is our class EventHandler
Async part and Queue of this pattern implemented by JavaNIO.
We just interact with queue by calling method from Java NIO   Selector.selectedKeys();  (see my class Reactor)
It gives us a set of EventHandlers which have new events.
Queue contain event marks instead of messages and in EventHandler we need to read message from stream.
Therefore it's not very clear HalfSync/HalfAsync pattern, but one more queue will be redundant.

------------------------------------------------------------------------------------------------------------------------
----- Code
------------------------------------------------------------------------------------------------------------------------
Some additional info concerning code you can find on StackOverFlow (thread started by me)
http://stackoverflow.com/questions/16135487/java-nio-close-socketchannel-but-leave-serversocketchannel-opened

and here (blog of one of the developers which code was used as prototype for this assignment)
http://jeewanthad.blogspot.ru/2013/02/reactor-pattern-explained-part-1.html