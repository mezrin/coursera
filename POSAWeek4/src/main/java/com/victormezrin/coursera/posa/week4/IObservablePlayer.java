package com.victormezrin.coursera.posa.week4;

/**
 * User: Victor Mezrin
 * Date: 2013-04-04
 * Time: 3:36 PM
 */
interface IObservablePlayer {

    /**
     * Sets new observer for event "player hits the ball"
     *
     * @param observer Concrete observer
     */
    public void attachObserver(IPlayerObserver observer);
}
