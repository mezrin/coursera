package com.victormezrin.coursera.posa.week5;

/**
 * User: Victor Mezrin
 * Date: 2013-04-14
 * Time: 11:53 PM
 */
public class Philosopher implements Runnable {
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted() && this.eatingLeft > 0) {
            this.manager.acquireLeftChopstick(this);
            System.out.println(this.name + " picks up left chopstick.");
            this.manager.acquireRightChopstick(this);
            System.out.println(this.name + " picks up right chopstick.");
            this.doEat();
            this.manager.releaseLeftChopstick(this);
            System.out.println(this.name + " puts down left chopstick.");
            this.manager.releaseRightChopstick(this);
            System.out.println(this.name + " puts down right chopstick.");

            //added just for clarity - when thread sleeps another threads have a chance to lock chopsticks
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEatingLeft(int eatingLeft) {
        this.eatingLeft = eatingLeft;
    }

    public void setManager(ChopstickManager manager) {
        this.manager = manager;
    }

    private void doEat() {
        System.out.println(this.name + " eats.");
        this.eatingLeft--;
    }

    private String name;
    private int eatingLeft;
    private ChopstickManager manager;
}
